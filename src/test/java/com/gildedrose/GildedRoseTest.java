package com.gildedrose;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

public class GildedRoseTest {

	/**
	 * - At the end of each day our system lowers both values for every item
	 */
	@Test
	public void testItemQualityDegrades() {
		Item[] items = new Item[] { new Item("foo", 2, 5) };
		GildedRose app = new GildedRose(items);
		app.updateQuality();
		assertEquals(4, items[0].quality);
		assertEquals(1, items[0].sellIn);
		app.updateQuality();
		assertEquals(3, items[0].quality);
		assertEquals(0, items[0].sellIn);
	}

	/**
	 * Test Rule 1 - Once the sell by date has passed, Quality degrades twice as
	 * fast
	 */
	@Test
	public void testOnceTheSellByDateHasPassedQualityDegradesTwiceAsFast() {
		Item[] items = new Item[] { new Item("foo", 0, 5) };
		GildedRose app = new GildedRose(items);
		app.updateQuality();
		assertEquals(3, items[0].quality);
	}

	/**
	 * - The Quality of an item is never negative
	 */
	@Test
	public void testItemHasQualityNeverNegative() {
		// Item item = new Item("foo", 3, -1);
		// assertEquals(0, item.quality);
		// this case is violated by Item class.

		Item item = new Item("foo", 3, 1);
		Item[] items = { item };
		GildedRose app = new GildedRose(items);
		app.updateQuality();
		assertEquals(0, items[0].quality);
		app.updateQuality();
		assertEquals(0, items[0].quality);
		app.updateQuality();
		assertEquals(0, items[0].quality);

	}

	/**
	 * - "Aged Brie" actually increases in Quality the older it gets
	 */
	@Test
	public void testAgedBrieIncreasesQualityGettingOlder() {
		Item item = new Item("Aged Brie", 3, 5);
		assertEquals(5, item.quality);
		// this case is violated by Item class.

		Item[] items = { item };
		GildedRose app = new GildedRose(items);
		app.updateQuality();
		assertEquals(6, items[0].quality);
		app.updateQuality();
		assertEquals(7, items[0].quality);
	}

	/**
	 * - The Quality of an item is never more than 50
	 */
	@Test
	public void testItemQualityLimit() {
		Item item = new Item("Aged Brie", 3, 50);
		assertEquals(50, item.quality);
		Item[] items = { item };
		GildedRose app = new GildedRose(items);
		app.updateQuality();
		assertEquals(50, items[0].quality);
	}

	/**
	 * - "Sulfuras", being a legendary item, never has to be sold or decreases
	 * in Quality however "Sulfuras" is a legendary item and as such its Quality
	 * is 80 and it never alters.
	 */
	@Test
	public void testSulfurasNeverDecreasesQuality() {
		Item item = new Item("Sulfuras, Hand of Ragnaros", 3, 80);
		assertEquals(80, item.quality);
		Item[] items = { item };
		GildedRose app = new GildedRose(items);
		app.updateQuality();
		assertEquals(80, items[0].quality);
		app.updateQuality();
		assertEquals(80, items[0].quality);
	}

	/**
	 * - "Backstage passes", like aged brie, increases in Quality as its SellIn
	 * value approaches; Quality increases by 2 when there are 10 days or less
	 * and by 3 when there are 5 days or less but
	 * 
	 * actual: item name "Backstage passes to a TAFKAL80ETC concert"
	 * 
	 */
	@Test
	public void testBackstageIncreaseQuality() {
		Item item = new Item("Backstage passes to a TAFKAL80ETC concert", 12, 5);
		assertEquals(5, item.quality);
		Item[] items = { item };
		GildedRose app = new GildedRose(items);
		app.updateQuality();
		assertEquals(6, items[0].quality); // increased by 1
		assertEquals(11, items[0].sellIn);
		app.updateQuality();
		assertEquals(7, items[0].quality);
		assertEquals(10, items[0].sellIn);
		app.updateQuality();
		assertEquals(9, items[0].quality); // increased by 2
		assertEquals(9, items[0].sellIn);
		app.updateQuality();
		assertEquals(11, items[0].quality);
		assertEquals(8, items[0].sellIn);
		app.updateQuality();
		app.updateQuality();
		assertEquals(15, items[0].quality);
		assertEquals(6, items[0].sellIn);
		app.updateQuality();
		assertEquals(17, items[0].quality);
		assertEquals(5, items[0].sellIn);
		app.updateQuality();
		assertEquals(20, items[0].quality);// increased by 3
		assertEquals(4, items[0].sellIn);
		app.updateQuality();
		assertEquals(23, items[0].quality);
		assertEquals(3, items[0].sellIn);
	}

	/*
	 * Quality drops to 0 after the concert
	 */
	@Test
	public void testBackstageQualityDropToZeroAfterConcert() {
		Item item = new Item("Backstage passes to a TAFKAL80ETC concert", 2, 10);
		assertEquals(10, item.quality);
		Item[] items = { item };
		GildedRose app = new GildedRose(items);
		app.updateQuality();
		assertEquals(13, items[0].quality);
		assertEquals(1, items[0].sellIn);
		app.updateQuality();
		assertEquals(16, items[0].quality);
		assertEquals(0, items[0].sellIn);
		app.updateQuality();
		assertEquals(0, items[0].quality);
		assertEquals(-1, items[0].sellIn);
	}

	/*
	 * an item can never have its Quality increase above 50, except "Sulfuras"
	 */
	@Test
	public void testItemIncreaseQualityLimit() {
		Item item = new Item("Backstage passes to a TAFKAL80ETC concert", 200, 49);
		assertEquals(49, item.quality);
		Item[] items = { item };
		GildedRose app = new GildedRose(items);
		app.updateQuality();
		assertEquals(50, items[0].quality);
		assertEquals(199, items[0].sellIn);
		app.updateQuality();
		assertEquals(50, items[0].quality);
		assertEquals(198, items[0].sellIn);
	}

	/*
	 * "Conjured" items degrade in Quality twice as fast as normal items
	 */
	@Test
	public void testItemConjuredDecreaseQualityLimit() {
		Item item = new Item("Conjured", 200, 49);
		assertEquals(49, item.quality);
		Item[] items = { item };
		GildedRose app = new GildedRose(items);
		app.updateQuality();
		assertEquals(47, items[0].quality);
		assertEquals(199, items[0].sellIn);
	}

	@Test
	public void testFixture() {

		Item[] fixture = new Item[] { new Item("+5 Dexterity Vest", 10, 20), //
				new Item("Aged Brie", 2, 0), //
				new Item("Elixir of the Mongoose", 5, 7), //
				new Item("Sulfuras, Hand of Ragnaros", 0, 80), //
				new Item("Sulfuras, Hand of Ragnaros", -1, 80),
				new Item("Backstage passes to a TAFKAL80ETC concert", 15, 20),
				new Item("Backstage passes to a TAFKAL80ETC concert", 10, 49),
				new Item("Backstage passes to a TAFKAL80ETC concert", 5, 49),
				// this conjured item does not work properly yet
				new Item("Conjured Mana Cake", 3, 6) };

		GildedRose app = new GildedRose(fixture);
		assertEquals("+5 Dexterity Vest, 10, 20", fixture[0].toString());
		assertEquals("Aged Brie, 2, 0", fixture[1].toString());
		assertEquals("Elixir of the Mongoose, 5, 7", fixture[2].toString());
		assertEquals("Sulfuras, Hand of Ragnaros, 0, 80", fixture[3].toString());
		assertEquals("Sulfuras, Hand of Ragnaros, -1, 80", fixture[4].toString());
		assertEquals("Backstage passes to a TAFKAL80ETC concert, 15, 20", fixture[5].toString());
		assertEquals("Backstage passes to a TAFKAL80ETC concert, 10, 49", fixture[6].toString());
		assertEquals("Backstage passes to a TAFKAL80ETC concert, 5, 49", fixture[7].toString());
		assertEquals("Conjured Mana Cake, 3, 6", fixture[8].toString());

		// day 1
		app.updateQuality();
		assertEquals("+5 Dexterity Vest, 9, 19", fixture[0].toString());
		assertEquals("Aged Brie, 1, 1", fixture[1].toString());
		assertEquals("Elixir of the Mongoose, 4, 6", fixture[2].toString());
		assertEquals("Sulfuras, Hand of Ragnaros, 0, 80", fixture[3].toString());
		assertEquals("Sulfuras, Hand of Ragnaros, -1, 80", fixture[4].toString());
		assertEquals("Backstage passes to a TAFKAL80ETC concert, 14, 21", fixture[5].toString());
		assertEquals("Backstage passes to a TAFKAL80ETC concert, 9, 50", fixture[6].toString());
		assertEquals("Backstage passes to a TAFKAL80ETC concert, 4, 50", fixture[7].toString());
		assertEquals("Conjured Mana Cake, 2, 4", fixture[8].toString());

	}
}
