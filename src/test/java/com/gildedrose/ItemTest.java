package com.gildedrose;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

public class ItemTest {
	/**
	 * - All items have a SellIn value which denotes the number of days we have
	 * to sell the item
	 */
	@Test
	public void testItemHasASellIn() {
		Item item = new Item("foo", 3, 5);
		assertEquals(3, item.sellIn);
	}

	/**
	 * - All items have a Quality value which denotes how valuable the item is
	 */
	@Test
	public void testItemHasAQuality() {
		Item item = new Item("foo", 3, 5);
		assertEquals(5, item.quality);
	}

}
