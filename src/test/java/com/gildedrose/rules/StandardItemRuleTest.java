package com.gildedrose.rules;

import org.junit.Assert;
import org.junit.Test;

import com.gildedrose.Item;

public class StandardItemRuleTest {

	@Test
	public void testApplyQualityRule() {
		ItemRule rule = new StandardItemRule();

		// quality decrease by 1;
		Item item = new Item("foo", 5, 4);
		rule.applyQualityRule(item);
		Assert.assertEquals(3, item.quality);

		// quality decrease by 2 for sellIn < 0;
		item = new Item("foo", -1, 4);
		rule.applyQualityRule(item);
		Assert.assertEquals(2, item.quality);
	}

	@Test
	public void testApplySellInRule() {
		ItemRule rule = new StandardItemRule();

		// sellIn decrease by 1;
		Item item = new Item("Conjured", 5, 4);
		rule.applySellInRule(item);
		Assert.assertEquals(4, item.sellIn);
	}
}
