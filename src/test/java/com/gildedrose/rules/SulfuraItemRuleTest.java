package com.gildedrose.rules;

import org.junit.Assert;
import org.junit.Test;

import com.gildedrose.Item;

public class SulfuraItemRuleTest {

	@Test
	public void testApplyQualityRule() {
		ItemRule rule = new SulfurasItemRule();

		// quality is 80 ;
		Item item = new Item("Sulfuras", 5, 60);
		rule.applyQualityRule(item);
		Assert.assertEquals(80, item.quality);

	}

	@Test
	public void testApplySellInRule() {
		ItemRule rule = new SulfurasItemRule();

		// sellIn never alters;
		Item item = new Item("Sulfuras", 5, 4);
		rule.applySellInRule(item);
		Assert.assertEquals(5, item.sellIn);
	}
}
