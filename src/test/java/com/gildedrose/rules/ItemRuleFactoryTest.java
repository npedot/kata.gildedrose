package com.gildedrose.rules;

import org.hamcrest.CoreMatchers;
import org.junit.Assert;
import org.junit.Test;

import com.gildedrose.Item;

public class ItemRuleFactoryTest {

	@Test
	public void testGetItemRule() {

		// Any item with a name starting with Conjured triggers a
		// ConjuredItemRule.
		Item item = new Item("Conjured XYZ", 5, 5);
		ItemRule rule = ItemRuleFactory.getItemRule(item);
		Assert.assertThat(rule, CoreMatchers.instanceOf(ConjuredItemRule.class));

		// Any item with a name starting with Backstage triggers a
		// BackstageItemRule.
		item = new Item("Backstage XYZ", 5, 5);
		rule = ItemRuleFactory.getItemRule(item);
		Assert.assertThat(rule, CoreMatchers.instanceOf(BackstageItemRule.class));

		// Any item with a name starting with Sulfuras triggers a
		// SulfurasItemRule.
		item = new Item("Sulfuras XYZ", 5, 5);
		rule = ItemRuleFactory.getItemRule(item);
		Assert.assertThat(rule, CoreMatchers.instanceOf(SulfurasItemRule.class));

		// Any item with a name "Aged Brie" triggers an
		// AgedBrieItemRule.
		item = new Item("Aged Brie", 5, 5);
		rule = ItemRuleFactory.getItemRule(item);
		Assert.assertThat(rule, CoreMatchers.instanceOf(AgedBrieItemRule.class));

		// An item with any other name triggers a
		// StandardItemRule.
		item = new Item("X Y Z", 5, 5);
		rule = ItemRuleFactory.getItemRule(item);
		Assert.assertThat(rule, CoreMatchers.instanceOf(StandardItemRule.class));

	}
}
