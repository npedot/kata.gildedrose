package com.gildedrose.rules;

import org.junit.Assert;
import org.junit.Test;

import com.gildedrose.Item;

public class BackstageItemRuleTest {

	@Test
	public void testApplyQualityRule() {
		ItemRule rule = new BackstageItemRule();

		// quality increase by 2 when there are 10 days or less;
		Item item = new Item("Backstage", 5, 4);
		rule.applyQualityRule(item);
		Assert.assertEquals(6, item.quality);

		// quality increase by 3 when there are 5 days or less;
		item = new Item("Backstage", 3, 4);
		rule.applyQualityRule(item);
		Assert.assertEquals(7, item.quality);

		// quality increase by 1;
		item = new Item("Backstage", 15, 4);
		rule.applyQualityRule(item);
		Assert.assertEquals(5, item.quality);
	}

	@Test
	public void testApplySellInRule() {
		ItemRule rule = new BackstageItemRule();

		// sellIn decrease by 1;
		Item item = new Item("Backstage", 5, 4);
		rule.applySellInRule(item);
		Assert.assertEquals(4, item.sellIn);
	}
}
