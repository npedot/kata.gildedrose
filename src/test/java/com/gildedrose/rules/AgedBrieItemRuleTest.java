package com.gildedrose.rules;

import org.junit.Assert;
import org.junit.Test;

import com.gildedrose.Item;

public class AgedBrieItemRuleTest {

	@Test
	public void testApplyQualityRule() {
		ItemRule rule = new AgedBrieItemRule();

		// quality increase by 1
		Item item = new Item("Aged Brie", 5, 4);
		rule.applyQualityRule(item);
		Assert.assertEquals(5, item.quality);

		// max increase is 50
		item = new Item("Aged Brie", 5, 50);
		rule.applyQualityRule(item);
		Assert.assertEquals(50, item.quality);

	}

	@Test
	public void testApplySellInRule() {
		ItemRule rule = new AgedBrieItemRule();

		// sellIn decrease by 1;
		Item item = new Item("Aged Brie", 5, 4);
		rule.applySellInRule(item);
		Assert.assertEquals(4, item.sellIn);
	}
}
