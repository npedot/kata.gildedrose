package com.gildedrose.rules;

import org.junit.Assert;
import org.junit.Test;

import com.gildedrose.Item;

public class ConjuredItemRuleTest {

	@Test
	public void testApplyQualityRule() {
		ItemRule rule = new ConjuredItemRule();

		// quality decrease by 2;
		Item item = new Item("Conjured", 5, 4);
		rule.applyQualityRule(item);
		Assert.assertEquals(2, item.quality);

		// quality decrease by 4 for sellIn < 0;
		item = new Item("Conjured", -1, 5);
		rule.applyQualityRule(item);
		Assert.assertEquals(1, item.quality);

	}

	@Test
	public void testApplySellInRule() {
		ItemRule rule = new ConjuredItemRule();

		// sellIn decrease by 1;
		Item item = new Item("Conjured", 5, 4);
		rule.applySellInRule(item);
		Assert.assertEquals(4, item.sellIn);
	}
}
