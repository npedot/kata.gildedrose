package com.gildedrose.rules;

import com.gildedrose.Item;

/**
 * Given a Item fabrics an appropriate ItemRule.
 *
 */
public class ItemRuleFactory {

	public static ItemRule getItemRule(Item item) {
		// defensive check
		if (item.name == null)
			throw new IllegalArgumentException();

		if (item.name.equals("Aged Brie")) {
			return new AgedBrieItemRule();
		}
		if (item.name.startsWith("Backstage")) {
			return new BackstageItemRule();
		}
		if (item.name.startsWith("Conjured")) {
			return new ConjuredItemRule();
		}
		if (item.name.startsWith("Sulfuras")) {
			return new SulfurasItemRule();
		}
		return new StandardItemRule();
	}
}
