package com.gildedrose.rules;

import com.gildedrose.Item;

public interface ItemRule {

	public void applyQualityRule(Item item);

	public void applySellInRule(Item item);
}
