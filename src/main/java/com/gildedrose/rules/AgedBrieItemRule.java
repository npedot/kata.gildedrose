package com.gildedrose.rules;

import com.gildedrose.Item;

public class AgedBrieItemRule extends StandardItemRule implements ItemRule {

	/**
	 * "Aged Brie" quality increases by 1. Any item has 50 as quality upper
	 * limit, except Sulfuras.
	 */
	@Override
	public void applyQualityRule(Item item) {
		// quality increase by 1;
		item.quality++;
		// check upper quality limit;
		if (item.quality > 50)
			item.quality = 50;
	}

}
