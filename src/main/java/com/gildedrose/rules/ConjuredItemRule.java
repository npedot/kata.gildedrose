package com.gildedrose.rules;

import com.gildedrose.Item;

public class ConjuredItemRule extends StandardItemRule implements ItemRule {

	/**
	 * Conjured items degrade in Quality twice as fast as normal items.
	 */
	@Override
	public void applyQualityRule(Item item) {
		// Conjured items degrade in Quality twice as fast as normal items.
		StandardItemRule stdRule = new StandardItemRule();
		stdRule.applyQualityRule(item);
		stdRule.applyQualityRule(item);
	}

}
