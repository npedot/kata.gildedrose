package com.gildedrose.rules;

import com.gildedrose.Item;

public class StandardItemRule implements ItemRule {

	/**
	 * At the end of each day our system lowers quality for every item. The
	 * Quality of an item is never negative.
	 */
	public void applyQualityRule(Item item) {
		// quality decreases by 1;
		item.quality--;
		// Once the sell by date has passed, Quality degrades twice as fast;
		if (item.sellIn < 0)
			item.quality--;
		// check lower quality limit;
		if (item.quality < 0)
			item.quality = 0;
	}

	/**
	 * At the end of each day our system lowers sellIn for every item.
	 */
	public void applySellInRule(Item item) {
		// sellIn decrease by 1;
		item.sellIn--;
	}

}
