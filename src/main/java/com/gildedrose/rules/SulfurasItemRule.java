package com.gildedrose.rules;

import com.gildedrose.Item;

public class SulfurasItemRule extends StandardItemRule implements ItemRule {

	@Override
	/**
	 * "Sulfuras" is a legendary item and as such its Quality is 80 and it never
	 * alters.
	 */
	public void applyQualityRule(Item item) {
		item.quality = 80;
	}

	/**
	 * Sulfuras never alters.
	 */
	@Override
	public void applySellInRule(Item item) {
		// Never alters.
	}

}
