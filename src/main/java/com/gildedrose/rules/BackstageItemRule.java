package com.gildedrose.rules;

import com.gildedrose.Item;

public class BackstageItemRule extends StandardItemRule implements ItemRule {

	/**
	 * "Backstage passes", like aged brie, increases in Quality as its SellIn
	 * value approaches; Quality increases by 2 when there are 10 days or less
	 * and by 3 when there are 5 days or less.
	 */
	@Override
	public void applyQualityRule(Item item) {
		// quality increase by 3 when there are 5 days or less;
		if (item.sellIn < 5)
			item.quality += 3;
		// quality increase by 2 when there are 10 days or less;
		else if (item.sellIn < 10)
			item.quality += 2;
		// quality increase by 1;
		else
			item.quality += 1;
		// an item can never have its Quality increase above 50;
		if (item.quality > 50)
			item.quality = 50;
		// Quality drops to 0 after the concert;
		if (item.sellIn < 0)
			item.quality = 0;
	}

}
