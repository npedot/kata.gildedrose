package com.gildedrose;

import com.gildedrose.rules.ItemRule;
import com.gildedrose.rules.ItemRuleFactory;

/**
 * System to update inventary, collection of Item.
 * 
 */
class GildedRose {
	Item[] items;

	public GildedRose(Item[] items) {
		this.items = items;
	}

	/**
	 * To update inventary Quality call this method once a day.
	 */
	public void updateQuality() {
		for (int i = 0; i < items.length; i++) {
			updateItemQuality(items[i]);
		}

	}

	/**
	 * To update Quality of a single item call this method once a day.
	 * 
	 * @param item
	 */
	private void updateItemQuality(Item item) {
		ItemRule rule = ItemRuleFactory.getItemRule(item);
		rule.applySellInRule(item);
		rule.applyQualityRule(item);

	}
}
